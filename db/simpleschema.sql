
create table item(
  itemkey int not null auto_increment,
  nm varchar(10) not null,
  primary key (itemkey),
  constraint citmnm unique(nm)
  );
 create table cpty(
   cptykey int not null auto_increment,
   nm varchar(100) not null,
   primary key (cptykey),
   constraint ccptnm unique(nm)
 );
create table trdord(
   trdordkey int not null auto_increment,
   cptykey int not null,
   orderdate date not null,
   primary key (trdordkey),
   constraint ftoc foreign key (cptykey) references cpty(cptykey)
);
create table trdli(
  trdlikey int not null auto_increment,
  trdordkey int not null,
  itemkey int not null,
  quantity float not null,  
  primary key(trdlikey),
  constraint ftlto foreign key (trdordkey) references cpty(cptykey),
  constraint ftli foreign key (itemkey) references item(itemkey)
 );
 
