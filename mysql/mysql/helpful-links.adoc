= Helpful MySQL links

== Creating and using a database

https://dev.mysql.com/doc/refman/8.0/en/database-use.html[Create and Use DB]

https://dev.mysql.com/doc/workbench/en/wb-mysql-connections-new.html[Connection management]

https://www.mysql.com/downloads/[Downloads]

https://dev.mysql.com/downloads/file/?id=510038[Download a windows installer]



== JSON

https://dev.mysql.com/doc/refman/8.0/en/json.html[JSON datatype]

including *JSON Path Syntax*


https://dev.mysql.com/doc/refman/8.0/en/json-table-functions.html[JSON Table etc]


including *Nested path*


https://dev.mysql.com/blog-archive/json_table-the-best-of-both-worlds/[Handy examples JSON_TABLE, JSON_OBJECT, Nested Path]


https://www.digitalocean.com/community/tutorials/working-with-json-in-mysql[Helpful but with some PHP unpleasantness that can be ignored]

== Online scratchpads aka 'Fiddles'

https://dbfiddle.uk/?rdbms=mysql_8.0[dbfiddle mysql 8.0]

https://www.db-fiddle.com/[db-fiddle 8.0]

